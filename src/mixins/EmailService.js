import firebase from 'firebase';
import 'firebase/firestore';

export default {
  methods: {
    async writeEmailToDb(email) {
      const ref = firebase.firestore().collection('landing-page').doc();
      return ref.set({ email });
    },
  },
};
