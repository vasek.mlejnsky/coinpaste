import firebase from 'firebase';

export default {
  methods: {
    async createUser(email, password) {
      /* TODO: This method will be deprecated and will be updated
      to resolve with a firebase.auth.UserCredential */
      /* ref.: https://firebase.google.com/docs/reference/js/firebase.auth.Auth#createUserWithEmailAndPassword */
      return firebase.auth().createUserWithEmailAndPassword(email, password);
    },
    async signInUser(email, password) {
      /* TODO: This method will be deprecated and will be updated
      to resolve with a firebase.auth.UserCredential */
      /* ref.: https://firebase.google.com/docs/reference/js/firebase.auth.Auth#signInWithEmailAndPassword */
      return firebase.auth().signInWithEmailAndPassword(email, password);
    },
  },
};
