import Vue from 'vue';
import Vuex from 'vuex';

import currentUser from '@/store/currentUser';

Vue.use(Vuex);
export default new Vuex.Store({
  modules: {
    currentUser,
  },
});
