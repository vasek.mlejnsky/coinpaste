// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import VueRouter from 'vue-router';
import VueScrollTo from 'vue-scrollto';
import firebase from 'firebase';

import App from '@/App';
import store from '@/store';
import routes from '@/router/routes';

/* <Firebase config> */
const config = {
  apiKey: 'AIzaSyD1WL-8G6PyuFxaFXhdxRLY3WdnnB8gNFE',
  authDomain: 'coinpaste.firebaseapp.com',
  databaseURL: 'https://coinpaste.firebaseio.com',
  projectId: 'coinpaste',
  storageBucket: 'coinpaste.appspot.com',
  messagingSenderId: '541604402441',
};
firebase.initializeApp(config);
/* </Firebase config> */

/* <Router config> */
Vue.use(VueRouter);
const router = new VueRouter({
  routes,
  mode: 'history',
  /* mode: 'hash', */
});
/* </Router config> */


Vue.use(VueScrollTo);

/* <Vue config> */
Vue.config.productionTip = false;
// Line below makes sure to wait for Firebase to initialize
firebase.auth().onAuthStateChanged(() => {
  /* eslint-disable no-new */
  new Vue({
    el: '#app',
    store,
    router,
    components: { App },
    template: '<App/>',
  });
});
/* </Vue config> */
